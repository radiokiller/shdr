#!bin/bash

easy_installp pip

# install homebrew before install opencv
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

#install opencv via homebrew
brew tap homebrew/science
brew install opencv

#install all required libs
pip install numpy scipy theano h5py sklearn matplotlib scikit-image
pip install opencv-python
pip install tensorflow keras
